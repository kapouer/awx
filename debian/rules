#!/usr/bin/make -f

export DESTDIR=$(CURDIR)/debian/awx
export AWXHOME=/var/lib/awx
export DH_VIRTUALENV_INSTALL_ROOT=$(AWXHOME)/venv

ifeq ($(wildcard $(HOME)),)
export HOME=$(CURDIR)/tmp
endif

%:
	dh $@ --with python-virtualenv

override_dh_virtualenv:
	dh_virtualenv --builtin-venv --requirements requirements/requirements.txt --python python3 --upgrade-pip --install-suffix awx
	dh_virtualenv --builtin-venv --requirements debian/ansible_requirements.txt --python python3 --upgrade-pip --install-suffix ansible 
	# needed until https://github.com/spotify/dh-virtualenv/issues/305 is fixed
	cp debian/awx-manage $(DESTDIR)$(DH_VIRTUALENV_INSTALL_ROOT)/awx/bin/
	cp debian/awx-python $(DESTDIR)$(DH_VIRTUALENV_INSTALL_ROOT)/awx/bin/
	# production settings require this file to be removed
	find debian/awx -name devonly.py -delete
	# drop lots of those
	find debian/awx \( -name "LICENSE*" -o -name "LICENCE*" -o -name "README*" \) -delete
	# those are created by postinst instead
	find debian/awx -type d -name __pycache__ -exec rm -rf '{}' +
	# install static link without knowing python version
	mkdir -p $(DESTDIR)/usr/share/awx; \
	staticPath=`cd $(DESTDIR) && find var/lib/awx/venv/awx/lib -path "*/awx/ui_next/build/static"`; \
	ln -sT /$${staticPath} $(DESTDIR)/usr/share/awx/static

override_dh_installsystemd:
	dh_installsystemd --name=awx-cbreceiver
	dh_installsystemd --name=awx-dispatcher
	dh_installsystemd --name=awx-daphne
	dh_installsystemd --name=awx-web

override_dh_install:
	dh_install
	make ui-release
	rm -f $(DESTDIR)$(AWXHOME)/favicon.ico
	rm -f $(DESTDIR)$(AWXHOME)/wsgi.py

override_dh_auto_test:
	# pass

override_dh_clean:
	dh_clean
	- make clean
	rm -rf tmp

override_dh_dwz:
	# pass

