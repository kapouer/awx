# Initial Setup

## database

Create a postgres user and database:
```
sudo -u postgres createuser -S awx
sudo -u postgres createdb -O awx awx
```

Copy and fill out:
cp /etc/awx/conf.d/database.py.sample /etc/awx/conf.d/database.py

Copy and fill out:
cp /etc/awx/conf.d/cluster_id.py.sample /etc/awx/conf.d/cluster_id.py

Note that email configuration can be done using the web ui itself.


## migration

```
sudo -u awx awx-manage migrate
```

## create super user

```
sudo -u awx awx-manage createsuperuser
```

## instance provisioning

```
sudo -u awx awx-manage provision_instance --hostname=myhost
sudo -u awx awx-manage register_queue --queuename=tower --hostnames=myhost
```

To change an instance name:
```
sudo -u awx awx-manage unregister_queue --queuename=tower --hostnames=myhost
sudo -u awx awx-manage deprovision_instance --hostname=myhost
```
then reprovision.


## nginx config

/usr/share/doc/awx ships a sample config, change hostname and ssl setup there.

